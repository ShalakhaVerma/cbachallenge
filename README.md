# Getting Started
## Problem
-show a list of transactions corresponding to an account
-Dynamically pull down data from https://www.dropbox.com/s/inyr8o29shntk9w/exercise.json?dl=1
-On this screen, ensure that the account information on top scrolls off the screen
-Transactions should be grouped together by day in descending order
-Each transaction group shows a short date and relative date
-Unit tests of business logic
### Bonus
-Use coroutines
-Support for refreshing the transaction list by either pull to refresh or a button
-On a click on transaction item navigate to a new page to show all transaction details (use simple VD)
-Basic accessibility support for voice over and dynamic text size changes


### Environmental setup
- macOS version: Catalina 10.15.7
- Android Studio version: Arctic Fox|2020.3.1 Patch 1

## Architecture, Design Patterns and Other Documentation
The project is built using clean architecture. Entities(Models) -> Use Cases(Repository/NetworkLayer) -> Presentation(View Models/Builders) -> Views(Activity/Fragments)

The project has been made using Dagger, LiveData, Coroutines, Retrofit , DataBinding , ViewModel

The unit tests enjoys the use of dependency injection to inject stubs / mocks in the class being tested. Also tests livedata values.
Unit tests is only written for `AccountDetailViewModelImpl`

Ideally, views doesn't know about the models. Hence, display models are being used to map the data from models to UI.

ViewModels are used to keep the presentation logic out of the view.

Jetpack Navigation Component used to navigate between Activity and Fragments.

JetPack SwipeToRefresh library for pull to refresh functionality

## Things to improve
- Internet Check before api call
- Could have create a wrapper class around network response
- Could have added more unit test coverage in `AccountDetailViewModelImpl`
- Could have added more unit test for Util classed and function. For eg: TransactionDetailViewModelImpl,
  UiUtil,etc
- Account Number spacing
- Pending balance correct value
- "Pending:" text styling in transaction item row