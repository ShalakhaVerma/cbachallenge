package com.example.cba.binding

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cba.data.domain.AccountItem
import com.example.cba.data.domain.TransactionItem
import com.example.cba.databinding.AccountDetailHeaderBinding
import com.example.cba.databinding.AccountDetailItemRowBinding
import com.example.cba.databinding.AccountDetailTransactionRowBinding

class AccountRecyclerViewAdapter(
    private val context: Context,
    private val data: List<AccountItem>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val VIEW_ACCOUNT_DETAIL = 1
        const val VIEW_TRANSACTION = 2
        const val VIEW_HEADER = 3
    }

    inner class AccountDetailItemViewHolder(var itemBinding: AccountDetailItemRowBinding) :
        RecyclerView.ViewHolder(itemBinding.root)

    inner class AccountDetailTransactionViewHolder(val itemBinding: AccountDetailTransactionRowBinding) :
        RecyclerView.ViewHolder(itemBinding.root)

    inner class AccountDetailHeaderViewHolder(val itemBinding: AccountDetailHeaderBinding) :
        RecyclerView.ViewHolder(itemBinding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return if (viewType == VIEW_ACCOUNT_DETAIL) {
            val listItemBinding = AccountDetailItemRowBinding.inflate(inflater, parent, false)
            AccountDetailItemViewHolder(listItemBinding)
        } else  if (viewType == VIEW_TRANSACTION) {
            val listItemBinding =
                AccountDetailTransactionRowBinding.inflate(inflater, parent, false)
            AccountDetailTransactionViewHolder(listItemBinding)
        } else {
            val listItemBinding =
                AccountDetailHeaderBinding.inflate(inflater, parent, false)
            AccountDetailHeaderViewHolder(listItemBinding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val accountItem = data[position]

        if (accountItem is AccountItem.AccountDetailItem) {
            (holder as AccountDetailItemViewHolder).itemBinding.account = accountItem
            holder.itemBinding.executePendingBindings()
        } else if (accountItem is TransactionItem) {
            (holder as AccountDetailTransactionViewHolder).itemBinding.transaction = accountItem
            holder.itemBinding.executePendingBindings()
        }
        else if(accountItem is  AccountItem.Header) {
            (holder as AccountDetailHeaderViewHolder).itemBinding.header = accountItem
            holder.itemBinding.executePendingBindings()
        }
    }

    override fun getItemViewType(position: Int): Int {
        val accountItem = data[position]
        if (accountItem is AccountItem.AccountDetailItem) return VIEW_ACCOUNT_DETAIL
        else if (accountItem is AccountItem.Header) return VIEW_HEADER
        return VIEW_TRANSACTION
    }

    override fun getItemCount() = data.size

}