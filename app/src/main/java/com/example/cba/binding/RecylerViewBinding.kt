package com.example.cba.binding

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cba.data.domain.AccountItem

@BindingAdapter("bindList")
fun bindRecyclerViewList(view: RecyclerView, dataList: List<AccountItem>?) {

    if (dataList.isNullOrEmpty())
        return

    val layoutManager = view.layoutManager
    if (layoutManager == null)
        view.layoutManager = LinearLayoutManager(view.context)

    var adapter = view.adapter

    if (adapter == null) {
        adapter = AccountRecyclerViewAdapter(view.context, dataList)
        view.adapter = adapter
    }

}