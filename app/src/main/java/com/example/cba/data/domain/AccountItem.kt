package com.example.cba.data.domain

import com.example.cba.util.convertStringToDate
import com.example.cba.util.getNumberOfDaysUntilCurrentDate

sealed class AccountItem {

    data class AccountDetailItem(
        private val availableBalance: String,
        private val balance: String,
        val bsb: String,
        val accountNumber: String,
        val accountName: String
    ) : AccountItem() {
        val displayAvailableBalance: String = availableBalance.let { "$$it" }
        val displayBalance: String = balance.let { "$$it" }
        val displayPendingBalance: String =
            availableBalance.let { "$$it" } // Todo:This should be replaced with actual pending balance logic
    }

    data class Header(private val effectiveDate: String) : AccountItem() {
        val date = convertStringToDate(effectiveDate, "EEE dd MMM")
        val days = getNumberOfDaysUntilCurrentDate(effectiveDate).toString().plus(" days ago")
    }
}