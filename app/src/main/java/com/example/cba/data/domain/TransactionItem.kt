package com.example.cba.data.domain

import android.os.Parcelable
import androidx.core.text.HtmlCompat
import com.example.cba.util.getCategoryIcon
import kotlinx.parcelize.Parcelize

@Parcelize
data class TransactionItem(
    private val isPending: Boolean,
    private val description: String?,
    private val category: String?,
    private val amount: String?,
    val effectiveDate: String?,
    val id: String?,
    val onClick: (TransactionItem) -> Unit
): Parcelable, AccountItem() {

    val displayDescription: CharSequence? = description?.trim()
        ?.let { desc -> HtmlCompat.fromHtml(desc, HtmlCompat.FROM_HTML_MODE_COMPACT) }
        .let { if (isPending) "isPending: $it" else it }

    val categoryIcon: Int? = category?.let { getCategoryIcon(it) }

    private val negativeAmount =
        amount?.trim()?.substring(0, 1).plus("$").plus(amount?.substring(1, amount.length))

    val displayAmount =
        if (amount?.startsWith("-") == true) negativeAmount else "$${amount?.trim()}"

    fun onClick() {
        onClick(this)
    }
}