package com.example.cba.data.network

import com.google.gson.annotations.SerializedName

data class Account(
    @SerializedName("account")
    val accountDetails: AccountDetails,
    @SerializedName("transactions")
    val transactions: List<Transactions>
) {
    data class AccountDetails(
        @SerializedName("bsb")
        val bsb: String,
        @SerializedName("accountNumber")
        val accountNumber: String,
        @SerializedName("balance")
        val balance: String,
        @SerializedName("available")
        val availableBalance: String,
        @SerializedName("accountName")
        val accountName: String
    )

    data class Transactions(
        @SerializedName("amount")
        val amount: String,
        @SerializedName("id")
        val id: String,
        @SerializedName("isPending")
        val isPending: Boolean,
        @SerializedName("description")
        val description: String,
        @SerializedName("category")
        val category: String,
        @SerializedName("effectiveDate")
        val effectiveDate: String,
    )
}