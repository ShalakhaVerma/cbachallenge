package com.example.cba.data.network

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ApiService {

    @Headers("Accept: application/json")
    @GET("exercisehhj.jsond?")
    suspend fun getAccount(
        @Query("dl") query: Int
    ): Response<Account>

}