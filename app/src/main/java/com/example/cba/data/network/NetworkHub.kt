package com.example.cba.data.network

import retrofit2.HttpException
import javax.inject.Inject

class NetworkHub @Inject constructor(private val apiService: ApiService) {

    suspend fun getAccount(index: Int): Account {
        val response = apiService.getAccount(index)
        if (response.isSuccessful) {
            return response.body()!!
        } else throw HttpException(response)
    }
}