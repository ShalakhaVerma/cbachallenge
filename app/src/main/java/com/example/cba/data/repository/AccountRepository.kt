package com.example.cba.data.repository

import com.example.cba.data.network.Account

interface AccountRepository {

    suspend fun getAccount(index: Int): Account
}