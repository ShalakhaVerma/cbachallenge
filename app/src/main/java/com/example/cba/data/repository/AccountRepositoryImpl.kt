package com.example.cba.data.repository

import com.example.cba.data.network.Account
import com.example.cba.data.network.NetworkHub
import javax.inject.Inject

class AccountRepositoryImpl @Inject constructor(private val networkHub: NetworkHub) :
    AccountRepository {
    override suspend fun getAccount(index: Int): Account {
        return networkHub.getAccount(index)
    }
}