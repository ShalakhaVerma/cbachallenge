package com.example.cba.di.component

import com.example.cba.MainApplication
import com.example.cba.di.module.NetworkModule
import com.example.cba.di.module.RepositoryModule
import com.example.cba.di.module.UiBindingModule
import com.example.cba.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidSupportInjectionModule::class, UiBindingModule::class,
        NetworkModule::class, ViewModelModule::class, RepositoryModule::class]
)
interface AppComponent : AndroidInjector<MainApplication> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: MainApplication): Builder

        fun build(): AppComponent
    }

    override fun inject(app: MainApplication)

}