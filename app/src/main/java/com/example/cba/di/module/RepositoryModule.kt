package com.example.cba.di.module

import com.example.cba.data.repository.AccountRepository
import com.example.cba.data.repository.AccountRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {

    @Binds
    abstract fun bindAccountRepositoryImpl(accountRepositoryImpl: AccountRepositoryImpl): AccountRepository

}