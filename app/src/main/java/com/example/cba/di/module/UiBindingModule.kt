package com.example.cba.di.module

import com.example.cba.MainActivity
import com.example.cba.ui.account.AccountDetailFragment
import com.example.cba.ui.transaction.TransactionDetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class UiBindingModule {
    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeAccountDetailFragment(): AccountDetailFragment

    @ContributesAndroidInjector
    abstract fun contributeTransactionDetailFragment(): TransactionDetailFragment
}