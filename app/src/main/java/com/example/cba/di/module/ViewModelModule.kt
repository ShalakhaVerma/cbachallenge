package com.example.cba.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.cba.ViewModelFactory
import com.example.cba.ViewModelKey
import com.example.cba.ui.account.AccountDetailViewModelImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @ExperimentalStdlibApi
    @Binds
    @IntoMap
    @ViewModelKey(AccountDetailViewModelImpl::class)
    abstract fun bindAccountDetailViewModelImpl(viewModel: AccountDetailViewModelImpl): ViewModel

}