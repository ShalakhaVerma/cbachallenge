package com.example.cba.ui.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cba.databinding.AccountDetailFragmentBinding
import com.example.cba.util.CustomAlertDialog
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class AccountDetailFragment : DaggerFragment() {

    companion object {
        fun newInstance() = AccountDetailFragment()
    }

    private var _binding: AccountDetailFragmentBinding? = null
    private val binding get() = _binding!!


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: AccountDetailViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(AccountDetailViewModelImpl::class.java)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = AccountDetailFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.recylerView.addItemDecoration(
            DividerItemDecoration(
                requireActivity(),
                LinearLayoutManager.VERTICAL
            )
        )

        binding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.refreshAccounts()
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setToolBar()

        viewModel.isRefreshing.observe(viewLifecycleOwner) {
            if (!it && binding.swipeRefreshLayout.isRefreshing) {
                binding.swipeRefreshLayout.isRefreshing = false
            }
        }

        viewModel.navigateToTransactionDetails.observe(viewLifecycleOwner) {
            val action = it.getContentIfNotHandled()?.let { it1 ->
                AccountDetailFragmentDirections.actionAccountDetailFragmentToTransactionDetailFragment(
                    it1
                )
            }
            if (action != null) {
                findNavController().navigate(action)
            }
        }

        viewModel.showGenericError.observe(viewLifecycleOwner) {
            val dialog = CustomAlertDialog()
            dialog.show(requireActivity().supportFragmentManager, "CustomAlertDialog")
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setToolBar() {
        binding.toolbar.title = ""
        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.toolbar)
    }
}