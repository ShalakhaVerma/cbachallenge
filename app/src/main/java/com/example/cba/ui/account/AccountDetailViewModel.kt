package com.example.cba.ui.account

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.cba.data.domain.AccountItem
import com.example.cba.data.domain.TransactionItem
import com.example.cba.lifecycle.Event

abstract class AccountDetailViewModel : ViewModel() {
    abstract val isLoading: LiveData<Boolean>
    abstract val isRefreshing: LiveData<Boolean>
    abstract val isEmptyList: LiveData<Boolean>
    abstract val itemList: LiveData<List<AccountItem>>
    abstract val toolBarTitle: LiveData<String>
    abstract val navigateToTransactionDetails: LiveData<Event<TransactionItem>>
    abstract val showNoConnectionError: LiveData<Event<Unit>>
    abstract val showGenericError: LiveData<Event<Unit>>

    abstract fun refreshAccounts()
}