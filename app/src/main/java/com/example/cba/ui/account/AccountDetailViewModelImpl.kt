package com.example.cba.ui.account

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.cba.data.domain.AccountItem
import com.example.cba.data.domain.TransactionItem
import com.example.cba.data.repository.AccountRepository
import com.example.cba.lifecycle.Event
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

class AccountDetailViewModelImpl @Inject constructor(
    private val accountRepository: AccountRepository
) : AccountDetailViewModel() {
    override val isLoading = MutableLiveData(false)
    override val isRefreshing = MutableLiveData(false)
    override val itemList = MutableLiveData<List<AccountItem>>()
    override val isEmptyList = MutableLiveData(false)
    override val toolBarTitle = MutableLiveData<String>()
    override val navigateToTransactionDetails = MutableLiveData<Event<TransactionItem>>()
    override val showNoConnectionError = MutableLiveData<Event<Unit>>()
    override val showGenericError = MutableLiveData<Event<Unit>>()
    private lateinit var listOfAccountItem: MutableList<AccountItem>

    private val handler = CoroutineExceptionHandler { _, exception ->
        println("Exception thrown in the coroutine: $exception")
    }
    init {
        isLoading.value = true
        getAccounts()
    }

    private fun getAccounts() {
        isEmptyList.value = false
        val job = viewModelScope.launch(handler) {
                val result = accountRepository.getAccount(1)
                val accountDetails = result.accountDetails
                listOfAccountItem = mutableListOf()
                listOfAccountItem.add(accountDetails.let {
                    toolBarTitle.value = it.accountName
                    AccountItem.AccountDetailItem(
                        it.availableBalance,
                        it.balance,
                        it.bsb,
                        it.accountNumber,
                        it.accountName
                    )
                })

                val transactions = result.transactions
                val groupedItems = transactions.groupBy {  it.effectiveDate}

                for((key, value) in groupedItems) {
                    listOfAccountItem.add(AccountItem.Header(key))
                    listOfAccountItem.addAll(
                        value.map {
                            TransactionItem(
                                it.isPending,
                                it.description,
                                it.category,
                                it.amount,
                                it.effectiveDate,
                                it.id,
                                onClick = ::onTransactionClicked
                            )
                        }
                    )
                }
                itemList.value = listOfAccountItem
                isLoading.value = false
                isRefreshing.value = false
            }

        job.invokeOnCompletion { throwable ->
            if(throwable != null) {
                isLoading.value = false
                isRefreshing.value = false
                showGenericError.value = Event(Unit)
                isEmptyList.value = true
            }
        }
    }

    override fun refreshAccounts() {
        isRefreshing.value = true
        getAccounts()
    }

    private fun onTransactionClicked(item: TransactionItem) {
        // here the setting of event could be further simplified by creating inline util functions
        navigateToTransactionDetails.value = Event(item)
    }
}