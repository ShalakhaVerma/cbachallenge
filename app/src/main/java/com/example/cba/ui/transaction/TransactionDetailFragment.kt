package com.example.cba.ui.transaction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.cba.databinding.TransactionDetailFragmentBinding
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class TransactionDetailFragment : DaggerFragment() {

    private var _binding: TransactionDetailFragmentBinding? = null
    private val binding get() = _binding!!

    private val args by navArgs<TransactionDetailFragmentArgs>()

    @Inject
    lateinit var factory: TransactionDetailViewModelImpl.Factory

    private val viewModel: TransactionDetailViewModel by viewModels {
        factory.apply {
            transaction = args.transaction
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = TransactionDetailFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        setToolBar()

        return binding.root
    }

    private fun setToolBar() {
        binding.toolbar.title = ""
        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.toolbar)
    }
}