package com.example.cba.ui.transaction

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel

abstract class TransactionDetailViewModel: ViewModel(){
    abstract val categoryIcon: LiveData<Int?>
    abstract val description: LiveData<CharSequence?>
    abstract val amount: LiveData<String>
    abstract val date: LiveData<String?>
}