package com.example.cba.ui.transaction

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.map
import com.example.cba.data.domain.TransactionItem
import com.example.cba.util.convertStringToDate
import javax.inject.Inject

class TransactionDetailViewModelImpl private constructor(transaction: TransactionItem) :
    TransactionDetailViewModel() {
    private val transactionDetail = MutableLiveData<TransactionItem>()

    override val categoryIcon= transactionDetail.map { it.categoryIcon }
    override val description= transactionDetail.map { it.displayDescription }
    override val amount= transactionDetail.map { it.displayAmount }
    override val date= transactionDetail.map { it.effectiveDate?.let { effectiveDate ->
        convertStringToDate(
            effectiveDate,
            "EEE dd MMM"
        )
    } }

    class Factory @Inject constructor() : ViewModelProvider.Factory {
        lateinit var transaction: TransactionItem
        override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            TransactionDetailViewModelImpl(transaction) as T

    }

    init {
        transactionDetail.value = transaction
    }
}