package com.example.cba.util

const val BUSINESS = "business"
const val CARDS = "cards"
const val CASH = "cash"
const val CATEGORIES = "categories"
const val EATING_OUT = "eatingout"
const val EDUCATION = "education"
const val ENTERTAINMENT = "entertainment"
const val GROCERIES = "groceries"
const val SHOPPING = "shopping"
const val TRANSPORTATION = "transportation"
const val TRAVEL = "travel"
