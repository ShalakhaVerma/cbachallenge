package com.example.cba.util

import androidx.annotation.DrawableRes
import com.example.cba.R
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.*

@DrawableRes
    fun getCategoryIcon(key: String): Int {
        return CATEGORY_ICON_MAP[key.lowercase()] ?: R.drawable.icon_category_uncategorised
    }

    private val CATEGORY_ICON_MAP = mapOf(
        BUSINESS to R.drawable.icon_category_business,
        CARDS to R.drawable.icon_category_cards,
        CASH to R.drawable.icon_category_cash,
        CATEGORIES to R.drawable.icon_category_categories,
        EATING_OUT to R.drawable.icon_category_eating_out,
        EDUCATION to R.drawable.icon_category_education,
        ENTERTAINMENT to R.drawable.icon_category_entertainment,
        GROCERIES to R.drawable.icon_category_groceries,
        SHOPPING to R.drawable.icon_category_shopping,
        TRANSPORTATION to R.drawable.icon_category_transportation,
        TRAVEL to R.drawable.icon_category_travel
    )

 fun convertStringToDate(input: String, format: String): String {
     val srcFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH)
     val srcDate = srcFormatter.parse(input)
     val targetFormatter = DateTimeFormatter.ofPattern(format, Locale.ENGLISH)
    return  targetFormatter.format(srcDate).toString()
}

fun getNumberOfDaysUntilCurrentDate(input: String): Int {
   val currentDate =  LocalDate.now()
    val date1 = LocalDate.parse(input)
    return date1.until(currentDate, ChronoUnit.DAYS).toInt()
}