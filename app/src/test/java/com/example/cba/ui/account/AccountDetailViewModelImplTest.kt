package com.example.cba.ui.account

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.cba.data.domain.AccountItem
import com.example.cba.data.domain.TransactionItem
import com.example.cba.data.network.Account
import com.example.cba.data.repository.AccountRepository
import com.example.cba.lifecycle.Event
import com.example.cba.utils.CoroutineDispatcherRule
import com.example.cba.utils.getOrAwaitValue
import com.example.cba.utils.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.HttpException
import retrofit2.Response

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class AccountDetailViewModelImplTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val dispatcherRule = CoroutineDispatcherRule()

    @Mock
    lateinit var repository: AccountRepository

    lateinit var viewModel: AccountDetailViewModel

    private fun getViewModel() {
        viewModel = AccountDetailViewModelImpl(repository)
    }

    @Test
    fun `test different view items in the list`() = runBlockingTest {
        //mock
        `when`(repository.getAccount(1)).thenReturn(mockAccountSuccessResponse())
        //behave
        getViewModel()

        //assert
        viewModel.isLoading.value?.let { assertFalse(it) }
        val list = viewModel.itemList.value
        assertNotNull(list)
        assertEquals(5, list?.size)
        assertEquals("Complete Access", (list?.get(0) as AccountItem.AccountDetailItem).accountName)
        assertEquals("$226.76", (list[0] as AccountItem.AccountDetailItem).displayAvailableBalance)
        assertEquals("Sat 27 Feb", (list[1] as AccountItem.Header).date)
        assertEquals("EA0F1831-5665-4668-B537-A17B14ECF7EF", (list[2] as TransactionItem).id)
        assertEquals("Fri 26 Feb", (list[3] as AccountItem.Header).date)
        assertEquals("5BDD2DBF-AD82-491C-9695-AF55EB3F7E69", (list[4] as TransactionItem).id)
    }

    @Test
    fun `check account data gets displayed on screen load`() = runBlockingTest {
        //mock
        `when`(repository.getAccount(1)).thenReturn(mockAccountSuccessResponse())
        //behave
        getViewModel()
        //assert
        viewModel.isLoading.value?.let { assertFalse(it) }
        assertNotNull(viewModel.itemList.value)
        assertEquals(5, viewModel.itemList.value?.size)
    }

    @Test
    fun `check error dialog is shown when there is an exception`() = runBlockingTest {
        //mock
        `when`(repository.getAccount(1)).thenThrow(mockErrorResponse())
        //behave
        getViewModel()
        //assert
        viewModel.isLoading.value?.let { assertFalse(it) }
        assertNull(viewModel.itemList.value)
        assertTrue(viewModel.isEmptyList.getOrAwaitValue())

        //verify
        val showGenericErrorObserver: Observer<Event<Unit>> = mock()
        viewModel.showGenericError.observeForever(showGenericErrorObserver)
        verify(showGenericErrorObserver, times(1)).onChanged(any())
    }

    @Test
    fun `check account data gets displayed when pull to refresh is called`() = runBlockingTest {
        //mock initial load
        `when`(repository.getAccount(1)).thenThrow(mockErrorResponse()).thenReturn(mockAccountSuccessResponse())
        //behave
        getViewModel()
        //assert
        assertFalse(viewModel.isLoading.getOrAwaitValue())
        assertFalse(viewModel.isRefreshing.getOrAwaitValue())
        assertTrue(viewModel.isEmptyList.getOrAwaitValue())

        // refresh
        //behave
        viewModel.refreshAccounts()
        //assert
        viewModel.isLoading.value?.let { assertFalse(it) }
        assertNotNull(viewModel.itemList.value)
        assertEquals(5, viewModel.itemList.value?.size)

        val itemListObserver: Observer<List<AccountItem>> = mock()
        viewModel.itemList.observeForever(itemListObserver)
        verify(itemListObserver, times(1)).onChanged(any())
    }


    private fun mockAccountSuccessResponse(): Account {
        val accountDetails: Account.AccountDetails =
            Account.AccountDetails("062005", "17095888", "246.7", "226.76", "Complete Access")
        val transactions = listOf(
            Account.Transactions(
                "-5.17",
                "EA0F1831-5665-4668-B537-A17B14ECF7EF",
                true,
                "Membership (via Patreon) Internet",
                "entertainment",
                "2021-02-27"
            ),
            Account.Transactions(
                "-10.28",
                "5BDD2DBF-AD82-491C-9695-AF55EB3F7E69",
                false,
                "gog.com Warsaw POL ## POL MERCHANT",
                "entertainment",
                "2021-02-26"
            )
        )

        return Account(accountDetails, transactions)
    }

    private fun mockErrorResponse(): HttpException {
        val errorBody =
            "{\"errors\": [\"Unexpected parameter\"]}".toResponseBody("application/json".toMediaTypeOrNull())
        return HttpException(Response.error<Any>(422, errorBody))
    }
}